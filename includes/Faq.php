<?php
/**
 * Faq Post Type
 *
 * @package   Faq_Post_Type
 * @license   GPL-2.0+
 */

/**
 * Helper functions for the cat post type.
 *
 * @package Faq_Post_Type
 */
class Faq {

	/**
	 * Returns the question.
	 * @return string Formatted html
	 */
	public static function faq_question() {
		global $post;
		return get_the_title();
	}

	/**
	 * Echoes Question
	 * @return void
	 */
	public static function question() {
		echo self::faq_question();
	}

	public static function get_refund_policy() {
		return get_option('cc_faq_refund_policy');
	}
	public static function get_privacy_policy() {
		return get_option( 'cc_faq_privacy_policy' );
	}
	public static function get_shipping_policy() {
		return get_option( 'cc_faq_shipping_policy' );
	}
	public function get_title() {
		$title = get_option('cc_faq_display_title');
		// return ($title  ? $title : 'Frequently Asked Questions');
		return $title;
	}

	public static function refund_policy() {
		echo self::get_refund_policy();
	}
	public static function privacy_policy() {
		echo self::get_privacy_policy();
	}
	public static function shipping_policy() {
		echo self::get_shipping_policy();
	}
	public static function title() {
		echo self::get_title();
	}

}
