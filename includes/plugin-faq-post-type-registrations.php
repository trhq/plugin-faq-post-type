<?php
/**
 * Faq Post Type
 *
 * @package   Faq_Post_Type
 * @license   GPL-2.0+
 */

/**
 * Register post types and taxonomies.
 *
 * @package faq_Post_Type
 */
class faq_Post_Type_Registrations {

	public $post_type = 'cat-cafe-faq';

	public $taxonomies = array( 'faq-category' );

	public function init() {
		// Add the team post type and taxonomies
		add_action( 'init', array( $this, 'register' ) );
	}

	/**
	 * Initiate registrations of post type and taxonomies.
	 *
	 * @uses faq_Post_Type_Registrations::register_post_type()
	 */
	public function register() {
		$this->register_post_type();
	}

	/**
	 * Register the custom post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	protected function register_post_type() {
		$labels = array(
			'name'               => __( 'FAQ', 'cat-cafe-faq' ),
			'singular_name'      => __( 'Question', 'cat-cafe-faq' ),
			'add_new'            => __( 'Add Question', 'cat-cafe-faq' ),
			'add_new_item'       => __( 'Add Question', 'cat-cafe-faq' ),
			'edit_item'          => __( 'Edit Question', 'cat-cafe-faq' ),
			'new_item'           => __( 'New Question', 'cat-cafe-faq' ),
			'view_item'          => __( 'View Question', 'cat-cafe-faq' ),
			'search_items'       => __( 'Search Questions', 'cat-cafe-faq' ),
			'not_found'          => __( 'No questions found', 'cat-cafe-faq' ),
			'not_found_in_trash' => __( 'No questions in the trash', 'cat-cafe-faq' ),
		);

		$supports = array(
			'title',
			'editor',
			'revisions',
		);

		$args = array(
			'labels'            => $labels,
			'supports'          => $supports,
			'public'            => true,
			'has_archive'       => true,
			'capability_type'   => 'post',
			'rewrite'           => array( 'slug' => 'faq', 'with_front' => false, ), // Permalinks format
			'menu_position'     => 20,
			'menu_icon'         => 'dashicons-id',
			'show_in_nav_menus' => true,
			'show_in_menu'      => true,
		);

		$args = apply_filters( 'faq_post_type_args', $args );

		register_post_type( $this->post_type, $args );
	}
}
