<?php

class Faq_Settings {

	/**
	 * Adds the settings menu to the FAQ post menu.
	 */
	public static function settings_menu() {
	    add_submenu_page('edit.php?post_type=cat-cafe-faq',
	    	'FAQ Settings',
	    	'Settings',
	    	'edit_posts',
	    	basename(__FILE__),
	    	['faq_settings','settings_page']
	    );
	}

	/**
	 * Registers settings for the FAQ post type.
	 */
	public static function settings_register() {
		register_setting( 'cc_faq_settings_group', 'cc_faq_display_title');
		register_setting( 'cc_faq_settings_group', 'cc_faq_refund_policy');
		register_setting( 'cc_faq_settings_group', 'cc_faq_privacy_policy');
		register_setting( 'cc_faq_settings_group', 'cc_faq_shipping_policy');
	}

	/**
	 * Creates a wp_editor formatted for the FAQ settings page.
	 * @param  string $id 	The id of the option you want to
	 *                     	assign to the editor.
	 * @return wp_editor    TinyMCE Text Editor Instance.
	 */
	public static function  editor($id) {
		$editor_settings = [
			'textarea_rows' => 10,
			'media_buttons' => false,
			'textarea_name' =>$id,
		];
		wp_editor( get_option($id) , $id, $editor_settings );
	}

	/**
	 * Defines the admin settings page and form inputs.
	 * @return void
	 */
	function settings_page() {
		?>
		<h2>FAQ Settings</h2>
		<p style="font-size:1.5em; max-width: 85%"> You can change the FAQ title and the terms and conditions which are displayed for the Cat Cafe Melbourne by updating this form with your new content. <br> This can be viewed at: <br> <a href="/faq/"><?php echo site_url();?>/faq/</a></p>
		<form method="post" action="options.php" style="width:98%;">
			<?php
				settings_fields( 'cc_faq_settings_group' );
				do_settings_sections( 'cc_faq_settings_group' );
			?>
			<table class="form-table" style="max-width: 85%;" border="0">
				<tr>
					<th style="width: 20%;">Display Title</th>
					<td>
						<input type="text" name="cc_faq_display_title" value="<?php echo esc_attr( get_option('cc_faq_display_title') ); ?>" style="width:100%" >
					</td>
				</tr>
				<tr style="height: 1em;">
					<th>Refund Policy</th>
					<td rowspan="2"> <?php self::editor('cc_faq_refund_policy'); ?> </td>
				</tr>
				<tr> <td style="vertical-align:top;"> Add a refund policy to be displayed on the site. </td> </tr>
				<tr style="height: 1em;">
					<th> Privacy Policy </th>
					<td rowspan="2"><?php self::editor('cc_faq_privacy_policy'); ?></td>
				</tr>
				<tr> <td style="vertical-align:top;"> Add a privacy policy to be displayed on the site. </td> </tr>
				<tr style="height: 1em;">
					<th>Shipping Policy</th>
					<td rowspan="2"> <?php self::editor('cc_faq_shipping_policy'); ?> </td>
				</tr>
				<tr> <td style="vertical-align:top;"> Add a shipping policy to be displayed on the site. </td> </tr>
			</table>
			<?php submit_button(); ?>
		</form>
		<?php
	}

}

add_action( 'admin_init', ['faq_settings', 'settings_register'] );
add_action('admin_menu' , ['faq_settings', 'settings_menu']);


