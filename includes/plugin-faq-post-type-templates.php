<?php
/**
 * Faq Post Type
 *
 * @package   Faq_Post_Type
 * @license   All rights reserved
 */

/**
 * Register post types and taxonomies.
 *
 * @package Faq_Post_Type
 */
class Faq_Post_Type_Templates {

	public static function type() {
		return 'cat-cafe-faq';
	}

	public static function path() {
		return plugin_dir_path( __FILE__ ) . '..' . '/templates/';
	}

	public function __construct() {

	}

	/**
	 * Adds Filter for single Faq template.
	 */
	public static function single($template) {
	    global $post;

	    // Is this a "cat" post?
	    if ( $post->post_type == self::type() ){

	        $template_path = self::path() . 'single-faq.php';
	        // Return the custom template
	        if(file_exists($template_path)) return $template_path;
	    }

	    // Otherwise do nothing
	    return $template;
	}

	/**
	 * Add Filter for single cat template.
	 */
	public static function archive($template) {
	    global $post;

	    // Is this a "cat" post?
	    if ($post->post_type == self::type() ) {
	    	$template_path = self::path() . 'single-faq.php';

	        // Return the custom template
	        if(file_exists($template_path)) return $template_path;
	    }

	    // Otherwise, do nothing.
	    return $template;
	}

	/**
	 * Increases the number of cats to display on the archive.
	 * @param  WP_Query $query The Wordpress Query
	 * @return void        
	 */
	public static function archive_size( $query ) {
	    if ( is_post_type_archive( 'cat-cafe-faq' ) ) {
	        // Display 50 posts for a custom post type called 'movie'
	        $query->set( 'posts_per_page', 100 );
	        $query->set( 'orderby', 'ID' );
			$query->set( 'order', 'ASC' );
	        return;
	    }
	}
}


add_filter('single_template', ['Faq_Post_Type_Templates', 'single']);
add_filter('archive_template', ['Faq_Post_Type_Templates', 'archive']);
add_action( 'pre_get_posts', ['Faq_Post_Type_Templates','archive_size'], 1 );
add_action('pre_get_posts', ['Faq_Post_Type_Templates', 'archive'], 1 );