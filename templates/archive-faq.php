<?php get_template_part('templates/page', 'header'); ?>
<p> Here at Cat Cafe Melbourne we have 14 lovely felines just waiting for your love and affection.</p>

<p> We have included a little about their unique personalities along with their picture so that you know who your petting, and what your favourite ones name is!</p>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content-cat', get_post_type() != 'cat' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
