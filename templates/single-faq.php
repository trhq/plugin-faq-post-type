<h1><?php Faq::title(); ?></h1>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<article class="panel panel-default">
			<header 
				class="panel-heading" 
				role="tab" 
				style="position:relative"
				id="heading-<?php echo get_the_id(); ?>"
			>
				<h3 class="panel-title">
					<a role="button" 
						data-toggle="collapse" 
						data-parent="#accordion" 
						href="#collapse-<?php echo get_the_id(); ?>" 
						aria-expanded="false" 
						aria-controls="collapse-<?php echo get_the_id(); ?>"
					>
						<?php the_title(); ?>
					</a>
				</h3>
				<a role="button" 
					class="btn btn-default btn-sm"
					style="top: 4px; right: 4px; position: absolute;"
					data-toggle="collapse" 
					data-parent="#accordion" 
					href="#collapse-<?php echo get_the_id(); ?>" 
					aria-expanded="false" 
					aria-controls="collapse-<?php echo get_the_id(); ?>"
				>
					<span class="glyphicon glyphicon-question-sign"></span>
				</a>
			</header>
			<section id="collapse-<?php echo get_the_id(); ?>" 
				class="panel-collapse collapse" 
				role="tabpanel" 
				aria-labelledby="heading-<?php echo get_the_id(); ?>"
			>
				<div class="panel-body">
					<?php the_content(); ?>
				</div>
			</section>
		</article>

	<?php endwhile; endif; ?>
</div>
<article class="terms">
	<section>
		<h4> Refund Policy </h4>
		<?php Faq::refund_policy(); ?>
	</section>
	<section>
		<h4> Privacy Policy </h4>
		<?php Faq::privacy_policy(); ?>
	</section>
	<section>
		<h4> Shipping Policy </h4>
		<?php Faq::shipping_policy(); ?>
	</section>
</article>
<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<script>
$(document).ready(function(){
	$('.btn').bind('mouseover', function(){
		$(this).addClass('btn-primary');
	});
	$('.btn').bind('mouseout', function(){
		$(this).removeClass('btn-primary');
	});
});
</script>


